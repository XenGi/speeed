[![asciicast](demo.gif)](https://asciinema.org/a/149586)

# speeed

Ping like tool that measures packet speed instead of response time.

## Install

  - [Archlinux AUR](https://aur.archlinux.org/packages/python-speeed)
    ```
    $ pacaur -S python-speeed
    ```
  - [PyPi](https://pypi.python.org/pypi?:action=display&name=speeed)
    ```
    $ pip install --user speeed
    ```


Inspired by: http://blog.wesleyac.com/posts/ping-lightspeed
